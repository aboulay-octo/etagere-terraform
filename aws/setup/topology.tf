provider "aws" {
  version = "~> 2.0"
  region  = "eu-west-1"
}

resource "aws_vpc" "etagere" {
  cidr_block = "10.4.0.0/24"

  tags = {
    Name = "etagere"
  }
}

resource "aws_subnet" "etagere" {
  vpc_id     = aws_vpc.etagere.id
  cidr_block = "10.4.0.0/25"

  tags = {
    Name = "etagere"
  }
}

resource "aws_security_group" "etagere-default" {
  name        = "etagere-default"
  description = "Allow ssh and http"
  vpc_id      = aws_vpc.etagere.id

  ingress {
    from_port   = "4242"
    to_port     = "4242"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}